<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('blocks.contents.home');
});

Route::get('/main', function (){
    return view('blocks.contents.main');
});

Route::get('/categories', function (){
    return view('blocks.contents.categories');
});

Route::get('/category/{category-name}', function (){
    return view('blocks.contents.category');
});

Route::get('/product/{productname}', function (){
    return view('blocks.contents.product');
});

Route::get('/product/{productname}/description', function (){
    return view('blocks.contents.product.description');
});

Route::post('cart/add/product/{id}/{qty}', function () {
     return view('blocks.contents.cart');
});




Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
